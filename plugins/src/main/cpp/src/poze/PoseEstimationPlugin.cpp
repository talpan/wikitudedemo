//
//  PoseEstimationPlugin.cpp
//  DevApplication
//
//  Created by Matej Paradzik 16/11/17.
//  Copyright (c) 2017 Poze. All rights reserved.
//

#include "PoseEstimationPlugin.h"
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "opencv2/opencv.hpp"

#define LICENCE_KEY "48-49-50-49-62-52-63-42-56-60-45-51-50-50-60-100-100-48-57-56-55-51-52-102-60-61-48-102-109-48-52-49-51-98-52-50-51-97-63-59-97-49-103-48-52-99-51-97-105-62-49-100-54-48-60-103-96-99-56-57-99-96-55-98-51-55-100-63-59-104-51-50-58-96-48-97-54-53-110"

static void printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    LOGI("GL %s = %s\n", name, v);
}

static void checkGlError(const char* op) {
    for (GLint error = glGetError(); error; error
                                                    = glGetError()) {
        LOGI("after %s() glError (0x%x)\n", op, error);
    }
}

auto gVertexShader =
        "attribute vec4 vPosition;\n"
                "void main() {\n"
                "  gl_Position = vPosition;\n"
                "}\n";

auto gFragmentShader =
        "precision mediump float;\n"
                "void main() {\n"
                "  gl_FragColor = vec4(0.0/255.0, 210.0/255.0, 212.0/255.0, 1.0);\n"
                "}\n";

GLuint loadShader(GLenum shaderType, const char* pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    LOGE("gl", "Could not compile shader %d:\n%s\n",
                         shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

GLuint createProgram(const char* pVertexSource, const char* pFragmentSource) {
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!vertexShader) {
        return 0;
    }

    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!pixelShader) {
        return 0;
    }

    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");
        glAttachShader(program, pixelShader);
        checkGlError("glAttachShader");
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char* buf = (char*) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
                    LOGE("gl", "Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    return program;
}

GLuint gProgram;
GLuint gvPositionHandle;

bool setupGraphics(int w, int h) {
    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);

    gProgram = createProgram(gVertexShader, gFragmentShader);
    if (!gProgram) {
        LOGE("gl", "Could not create program.");
        return false;
    }
    gvPositionHandle = glGetAttribLocation(gProgram, "vPosition");
    checkGlError("glGetAttribLocation");
    LOGI("gl", "glGetAttribLocation(\"vPosition\") = %d\n",
         gvPositionHandle);

    return true;
}

const GLfloat gTriangleVertices[] = { 0.0f, 0.5f, -0.5f, -0.5f,
                                      0.5f, -0.5f };

PoseEstimationPlugin* PoseEstimationPlugin::instance;

extern "C" JNIEXPORT void JNICALL
Java_com_wikitude_samples_plugins_PoseEstimationPluginActivity_initNative(JNIEnv* env, jobject obj, jstring modelPath_)
{
    env->GetJavaVM(&pluginJavaVM);
    PoseEstimationPlugin::instance->initPoze(env->GetStringUTFChars(modelPath_, NULL));
}

extern "C" JNIEXPORT void JNICALL
Java_com_wikitude_samples_plugins_PoseEstimationPluginActivity_setIsBaseOrientationLandscape(JNIEnv* env, jobject obj, jboolean isBaseOrientationLandscape_)
{
    PoseEstimationPlugin::instance->setIsBaseOrientationLandscape((bool)isBaseOrientationLandscape_);
}

PoseEstimationPlugin::PoseEstimationPlugin(int cameraFrameWidth, int cameraFrameHeight) :
Plugin("tech.poze.wikitude.android.pozePlugin"),
_isBaseOrientationLandscape(false),
_cameraFrameWidth(cameraFrameWidth),
_cameraFrameHeight(cameraFrameHeight),
_interfaceOrientation(wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortrait)
{
    PoseEstimationPlugin::instance = this;
}

PoseEstimationPlugin::~PoseEstimationPlugin()
{
    delete _poze;
}

void PoseEstimationPlugin::initPoze(std::string modelPath)
{
    _poze = new Poze(modelPath, LICENCE_KEY);
}


void PoseEstimationPlugin::initialize() {
}

void PoseEstimationPlugin::destroy() {
}

void PoseEstimationPlugin::cameraFrameAvailable(const wikitude::sdk::Frame& cameraFrame_) {

    _scaledCameraHeight = cameraFrame_.getScaledHeight();
    _scaledCameraWidth = cameraFrame_.getScaledWidth();

    cv::Mat yuy(_cameraFrameHeight+_cameraFrameHeight/2, _cameraFrameWidth, CV_8UC1, (uchar *)cameraFrame_.getData());
    cv::Mat rgba(_cameraFrameHeight, _cameraFrameWidth, CV_8UC3);
    if(cameraFrame_.getFrameColorSpace() == wikitude::sdk::FrameColorSpace::YUV_420_YV12) {
        cv::cvtColor(yuy, rgba, CV_YUV2RGB_YV12);
    } else {
        cv::cvtColor(yuy, rgba, CV_YUV2RGB_NV21);
    }


    wikitude::sdk::InterfaceOrientation currentInterfaceOrientation;
    { // auto release scope
        std::unique_lock<std::mutex>(_interfaceOrientationMutex);

        currentInterfaceOrientation = _interfaceOrientation;
    }

    if (adjustInterfaceOrientation(currentInterfaceOrientation, _isBaseOrientationLandscape) == wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortrait) {
        cv::transpose(rgba, rgba);
        cv::flip(rgba, rgba, 1);
    }
    else if (adjustInterfaceOrientation(currentInterfaceOrientation, _isBaseOrientationLandscape) == wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortraitUpsideDown) {
        cv::transpose(rgba, rgba);
        cv::flip(rgba, rgba, 0);
    }
    else if (adjustInterfaceOrientation(currentInterfaceOrientation, _isBaseOrientationLandscape) == wikitude::sdk::InterfaceOrientation::InterfaceOrientationLandscapeLeft) {
        cv::flip(rgba, rgba, -1);
    }
    else if (adjustInterfaceOrientation(currentInterfaceOrientation, _isBaseOrientationLandscape) == wikitude::sdk::InterfaceOrientation::InterfaceOrientationLandscapeRight) {
        // nop for landscape right
    }

    if(_poze == NULL || !_poze->status_OK) {
        return;
    }

    _result.clear();
    _result = _poze->getSkeletons(rgba.data, rgba.cols, rgba.rows);
    LOGI("poze", "Found %d skeletons", (int)_result.size());
}

void PoseEstimationPlugin::update(const wikitude::sdk::RecognizedTargetsBucket& recognizedTargetsBucket_) {
}

void PoseEstimationPlugin::endRender() {

    if(!gProgram) {
        bool good = setupGraphics(0,0);
        if(!good) {
            return;
        }
    }

    if(_result.size() == 0) return;

    for(auto skeleton : _result) {

        static GLfloat _skeletonVertices[64];
        static GLushort _skeletonIndices[64];

        int numToDraw = skeleton.limbs.size() * 2;

        for (int i = 0; i < skeleton.limbs.size(); i++) {
            int idx = i * 4;
            _skeletonVertices[idx] = (2 * skeleton.limbs[i].relativeStartX - 1) * _scaledCameraWidth;
            _skeletonVertices[idx + 1] = (1 - 2 * skeleton.limbs[i].relativeStartY) * _scaledCameraHeight;
            _skeletonVertices[idx + 2] = (2 * skeleton.limbs[i].relativeEndX - 1) * _scaledCameraWidth;
            _skeletonVertices[idx + 3] = (1 - 2 * skeleton.limbs[i].relativeEndY) * _scaledCameraHeight;

            int idx2 = i * 2;
            _skeletonIndices[idx2] = idx2;
            _skeletonIndices[idx2 + 1] = idx2 + 1;
        }

        glUseProgram(gProgram);

        glDisable(GL_DEPTH_TEST);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glVertexAttribPointer(gvPositionHandle, 2, GL_FLOAT, GL_FALSE, 0, _skeletonVertices);
        glEnableVertexAttribArray(gvPositionHandle);

        glLineWidth(10.0f);
        glDrawElements(GL_LINES, numToDraw, GL_UNSIGNED_SHORT, _skeletonIndices);
    }
}

void PoseEstimationPlugin::surfaceChanged(wikitude::sdk::Size<int> renderSurfaceSize_, wikitude::sdk::Size<float> cameraSurfaceScaling_, wikitude::sdk::InterfaceOrientation interfaceOrientation_) {

    { // auto release scope
        std::unique_lock<std::mutex>(_interfaceOrientationMutex);

        _interfaceOrientation = interfaceOrientation_;
    }
}

wikitude::sdk::InterfaceOrientation PoseEstimationPlugin::adjustInterfaceOrientation(wikitude::sdk::InterfaceOrientation interfaceOrientation_, bool isBaseOrientationLandscape) {
    if (!isBaseOrientationLandscape) {
        return interfaceOrientation_;
    }
    else {
        switch (interfaceOrientation_) {
            case wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortrait:
                return wikitude::sdk::InterfaceOrientation::InterfaceOrientationLandscapeRight;
            case wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortraitUpsideDown:
                return wikitude::sdk::InterfaceOrientation::InterfaceOrientationLandscapeLeft;
            case wikitude::sdk::InterfaceOrientation::InterfaceOrientationLandscapeRight:
                return wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortraitUpsideDown;
            case wikitude::sdk::InterfaceOrientation::InterfaceOrientationLandscapeLeft:
                return wikitude::sdk::InterfaceOrientation::InterfaceOrientationPortrait;
        }
    }
}
