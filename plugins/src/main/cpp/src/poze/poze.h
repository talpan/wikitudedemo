//
// Created by neven on 16.09.17..
//

#ifndef NEURAL_NETWORK_POSE_ESTIMATION_POZE_H
#define NEURAL_NETWORK_POSE_ESTIMATION_POZE_H

#include <string>
#include <vector>

class PozeLimb {
public:
    float relativeStartX;
    float relativeStartY;

    float relativeEndX;
    float relativeEndY;

    std::string startName;
    std::string endName;
};

class PozeSkeleton {
public:
    std::vector<PozeLimb> limbs;

    PozeSkeleton(std::vector<PozeLimb> limbs_):
            limbs(limbs_)
    {}
};

class Poze {
public:
    int status_OK;

    Poze(std::string modelPath, std::string licence_key);
    ~Poze();

    std::vector<PozeSkeleton> getSkeletons(const unsigned char* rgbPtr, int width, int height);
};


#endif //NEURAL_NETWORK_POSE_ESTIMATION_POZE_H

