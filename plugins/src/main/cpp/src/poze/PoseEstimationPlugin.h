//
//  PoseEstimationPlugin.h
//  DevApplication
//
//  Created by Matej Paradzik 16/11/17.
//  Copyright (c) 2017 Poze. All rights reserved.
//

#ifndef __DevApplication__PoseEstimationPlugin__
#define __DevApplication__PoseEstimationPlugin__

#include "jni.h"

#include <Plugin.h>
#include <Frame.h>
#include <RecognizedTarget.h>
#include "poze.h"

#include <android/log.h>
#define  LOGI(LOG_TAG,...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(LOG_TAG,...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)


extern JavaVM* pluginJavaVM;

class PoseEstimationPlugin : public wikitude::sdk::Plugin {
public:
    PoseEstimationPlugin(int cameraFrameWidth, int cameraFrameHeight);
    virtual ~PoseEstimationPlugin();

    void initPoze(std::string modelPath);

    virtual void initialize();
    virtual void destroy();

    virtual void cameraFrameAvailable(const wikitude::sdk::Frame& cameraFrame_);
    virtual void update(const wikitude::sdk::RecognizedTargetsBucket& recognizedTargetsBucket_);
    virtual void endRender();

    virtual void surfaceChanged(wikitude::sdk::Size<int> renderSurfaceSize_, wikitude::sdk::Size<float> cameraSurfaceScaling_, wikitude::sdk::InterfaceOrientation interfaceOrientation_);

    inline void setIsBaseOrientationLandscape(bool isBaseOrientationLandscape_) {_isBaseOrientationLandscape = isBaseOrientationLandscape_;}
    static wikitude::sdk::InterfaceOrientation adjustInterfaceOrientation(wikitude::sdk::InterfaceOrientation interfaceOrientation_, bool isBaseOrientationLandscape);

protected:
    bool _isBaseOrientationLandscape;

    std::vector<PozeSkeleton> _result;

private:
    Poze* _poze;

public:
    static PoseEstimationPlugin* instance;

    int _cameraFrameWidth;
    int _cameraFrameHeight;

    float _scaledCameraWidth;
    float _scaledCameraHeight;

    wikitude::sdk::InterfaceOrientation _interfaceOrientation;
};

#endif /* defined(__DevApplication__PoseEstimationPlugin__) */
